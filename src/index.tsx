// Importing required components from the UI kit
import ForgeUI, {Fragment, MacroConfig, render, Text, TextField, useAction, useConfig} from '@forge/ui';
import {LogLevel, WebClient} from '@slack/web-api';
import {Message} from '@slack/web-api/dist/response/ImHistoryResponse';
import {User} from '@slack/web-api/dist/response/UsersInfoResponse';


const getMessagesFromThread = async (channelId, ts): Promise<Message[]> => {
    console.log("Making Slack API call...")
    const response = await client.conversations.replies({
        channel: channelId,
        ts: ts
    });
    return response.messages;
};


const getMessagesFromChannel = async (channelId): Promise<Message[]> => {
    console.log("Making Slack API call...")
    const response = await client.conversation.history({
        channel: channelId
    });
    return response.messages;
};


const getUsers = async (): Promise<User[]> => {
    console.log("Making Slack API call...")
    const response = await client.users.list();
    console.log("users: " + response.members)
    return response.members;
};

// ImageCardProps interface which will be used by ImageCard component
interface MessageCardProps {
    user: string;
    text: string;
}

// ImageCard component containing text and image
const MessageCard = ({user, text}: MessageCardProps) => (
    <Fragment>
        <Text>{user}: {text}</Text>
    </Fragment>
);

interface MessageCardsProps {
    messages: Message[];
}

// ImageCard component containing text and image
function ImageCards({messages}: MessageCardsProps) {
    const messageList = messages.map((message) => <MessageCard user={message.user} text={message.text}/>);
    return (
        <Fragment>{messageList}</Fragment>
    )
}

function replaceUserName(messages, users) {
    users.forEach(user => messages.filter(message => message.user === user.id).forEach(msg => msg.user = user.real_name))
    return messages;
}

const defaultConfig = {
    url: ""
};

let client;

const App = () => {
    const config = useConfig() || defaultConfig;
    client = new WebClient(process.env.SLACK_TOKEN, {
        // LogLevel can be imported and used to make debugging simpler
        logLevel: LogLevel.DEBUG
    });

    let channelId = config.url.split('/')[4];
    let ts = config.url.split('/')[5];
    ts = [ts.slice(1, 11), ".", ts.slice(11)].join('');

    const [users] = useAction(getUsers, getUsers);
    const [messages] = useAction(getMessagesFromThread, getMessagesFromThread(channelId, ts));



    return (
        <Fragment>
            <Text>Latest messages!</Text>
            <ImageCards messages={replaceUserName(messages, users)}/>
        </Fragment>
    );
};

// Exporting the above App function by exporting via 'run'
export const run = render(<App/>);

// Function that defines the configuration UI
const Config = () => {
    return (
        <MacroConfig>
            <TextField name="url" label="Message URL" defaultValue={defaultConfig.url}/>
        </MacroConfig>
    );
};

export const config = render(<Config/>);